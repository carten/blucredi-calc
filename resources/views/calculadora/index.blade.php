@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" id="app">
                    <div class="panel-heading">Calculadora para consórcio Blucredi</div>
                    <div class="panel-body">
                        <form action="" class="form-horizontal">


                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Tipo</label>
                                <div class="col-sm-10">
                                    <select v-model="tipo.selected" class="form-control" v-on:change="getMarcas()">
                                        <option v-for="opt in tipo.options" v-bind:value="opt.value">
                                            @{{ opt.label }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Marca</label>
                                <div class="col-sm-10">
                                    <select v-model="marca.selected" class="form-control" v-on:change="getModelos()">
                                        <option v-for="opt in marca.options" v-bind:value="opt.id">
                                            @{{ opt.fipe_name }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Modelo</label>
                                <div class="col-sm-10">
                                    <select v-model="modelo.selected" class="form-control" v-on:change="getFipe()">
                                        <option v-for="opt in modelo.options" v-bind:value="opt.id">
                                            @{{ opt.fipe_name }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Ano</label>
                                <div class="col-sm-10">
                                    <select v-model="fipe.selected" class="form-control" v-on:change="getCarro()">
                                        <option v-for="opt in fipe.options" v-bind:value="opt.id">
                                            @{{ opt.name }} - @{{ opt.veiculo }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Saldo Devedor</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" v-model="saldo_devedor">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Valor da carta</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" v-model="credito">
                                </div>
                            </div>

                            <div id="result" class="well">
                                <div v-if="carro">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <td>Ano</td>
                                            <td>@{{ carro.ano_modelo }}</td>
                                        </tr>
                                        <tr>
                                            <td>Cobustível</td>
                                            <td>@{{ carro.combustivel }}</td>
                                        </tr>
                                        <tr>
                                            <td>Preço</td>
                                            <td>@{{ carro.preco }}</td>
                                        </tr>
                                        <tr>
                                            <td>Refêrencia</td>
                                            <td>@{{ carro.referencia }}</td>
                                        </tr>
                                        <tr>
                                            <td>Valor necessário pelo ano</td>
                                            <td>@{{ valor_necessario | currency }}</td>
                                        </tr>
                                        <tr>
                                            <td>É possível de negociação?</td>
                                            <td>
                                                @{{ ehPossivel() }} <br>
                                                <p>@{{ message }}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div v-else>
                                    Selecione o carro acima
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var url_marcas  = '{{ url('/api/marcas') }}';
        var url_modelos = '{{ url('/api/modelos') }}';
        var url_fipe    = '{{ url('/api/fipe') }}';
        var url_carro    = '{{ url('/api/carro') }}';
        var calc = new Vue({
            el: '#app',
            data: {
                tipo: {
                    options: [
                        {
                            label: "Carros",
                            value: "carros"
                        },{
                            label: "Motos",
                            value: "motos"
                        },{
                            label: "Caminhões",
                            value: "caminhoes"
                        }
                    ],
                    selected: 'carros'
                },
                marca: {
                    options: [],
                    selected: null
                },
                modelo: {
                    options: [],
                    selected: null
                },
                fipe: {
                    options: [],
                    selected: null
                },
                carro: null,
                credito: 27840,
                saldo_devedor: 22504.06,
                valor_necessario: null,
                message: '',
                pc: 0
            },

            ready: function () {
                this.getMarcas();
            },

            methods: {
                getMarcas: function () {
                    var self = this;
                    $.get(url_marcas, {
                        tipo: self.tipo.selected
                    }, function (data) {
                        self.marca.options = JSON.parse(data);
                    });
                },

                getModelos: function () {
                    var self = this;
                    $.get(url_modelos, {
                        tipo: self.tipo.selected,
                        marca: self.marca.selected
                    }, function (data) {
                        self.modelo.options = JSON.parse(data);
                    });
                },

                getFipe: function () {
                    var self = this;
                    $.get(url_fipe, {
                        tipo: self.tipo.selected,
                        marca: self.marca.selected,
                        modelo: self.modelo.selected
                    }, function (data) {
                        self.fipe.options = JSON.parse(data);
                    });
                },

                getCarro: function () {
                    var self = this;
                    $.get(url_carro, {
                        tipo: self.tipo.selected,
                        marca: self.marca.selected,
                        modelo: self.modelo.selected,
                        carro: self.fipe.selected
                    }, function (data) {
                        self.carro = JSON.parse(data);
                        self.calcVlrNecessario();
                    });
                },

                calcVlrNecessario: function () {
                    var d = new Date();
                    var ano = d.getFullYear();

                    var idade = ano - Number(this.carro.ano_modelo);

                    if (idade < 7) {
                        this.pc = 30;
                    } else if (idade > 7 && idade < 10) {
                        this.pc = 50;
                    }

                    if (this.pc) {
                        var vlr = Number(this.saldo_devedor);

                        var result = vlr/100;
                        var total = vlr+(this.pc*result);

                        this.valor_necessario = total;
                    }
                },

                ehPossivel: function () {
                    if (!this.valor_necessario) {
                        this.message = 'Carro muito antigo, não rola, precisa ter pelo menos 10 anos de vida... :(';
                        return "Não";
                    }

                    var vlr = this.carro.preco.replace(/[^0-9.,]/gi, '');
                    vlr = vlr.replace('.', '');
                    vlr = Number(vlr.replace(',', '.'));

                    if (vlr >= this.valor_necessario) {
                        if (vlr > this.credito) {
                            this.message = 'É possível, porém o valor do carro é maior que seu crédito';
                            return 'Sim';
                        } else {
                            this.message = 'Tudo certo, dá-lhe!';
                            return 'Sim';
                        }
                    } else {
                        this.message = 'O Valor do carro precisa ser ' + this.pc +'% maior que o valor do fipe.';
                        return 'Não';
                    }
                }
            }
        })
    </script>
@endsection
