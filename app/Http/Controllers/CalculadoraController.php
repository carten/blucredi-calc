<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use anlutro\cURL\cURL;

class CalculadoraController extends Controller
{
    public function index() {
        return view('calculadora.index');
    }

    public function marcas(Request $request, cURL $curl) {
        if (!$request->has('tipo'))
            return 'Tipo não informado!';

        $url = 'http://fipeapi.appspot.com/api/1/'.$request->tipo.'/marcas.json';
        $response = $curl->jsonGet($url);
        return $response->body;
    }

    public function modelos(Request $request, cURL $curl) {
        if (!$request->has('marca'))
            return 'Marca não informada!';

        $url = 'http://fipeapi.appspot.com/api/1/'.$request->tipo.'/veiculos/'.$request->marca.'.json';
        $response = $curl->get($url);
        return $response->body;
    }

    public function fipe(Request $request, cURL $curl) {
        if (!$request->has('modelo'))
            return 'Modelo não informada!';

        $url = 'http://fipeapi.appspot.com/api/1/'.$request->tipo.'/veiculo/'.$request->marca.'/'.$request->modelo.'.json';
        $response = $curl->get($url);
        return $response->body;
    }

    public function carro(Request $request, cURL $curl) {
        if (!$request->has('carro'))
            return 'Carro não informada!';

        $url = 'http://fipeapi.appspot.com/api/1/'.$request->tipo.'/veiculo/'.$request->marca.'/'.$request->modelo.'/'.$request->carro.'.json';
        $response = $curl->get($url);
        return $response->body;
    }
}
